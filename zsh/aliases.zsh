alias ls="lsd"
alias ll="ls -Al"
alias l="ls -l"

alias lg="lazygit"

alias vim="nvim"
alias cim="vim"
alias v="nvim"
alias c="conf"
alias sc="scripts"

alias mkdir="mkdir -p"

alias zshrc="vim ~/.zshrc && source ~/.zshrc"

alias df="df -h"

alias grep="grep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}"

# I don't use just `.` for running scripts, so it's used for config
alias .='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'


alias n="npm run lint -- --quiet --fix&& npm run test -- --no-watch"
alias gp="git add . && git commit --amend --no-edit && git push --force"
alias ngp="n && gp"


alias glint='npx eslint $(git ls-files --exclude-standard --modified)'
