function poweroff(){
	if [[ -n $SSH_CONNECTION ]]; then
		echo "Connected to server." | toilet -f future
	else
		systemctl poweroff
	fi
}

function windows(){
	sudo bootctl set-oneshot auto-windows
	reboot
}

function vimrc(){
	pushd ~/.config/nvim &>/dev/null
	nvim
	popd &>/dev/null
}

function g(){
	files=$(find ~/dev -maxdepth 1 -mindepth 1 -type d)
	dir=`echo $files \
		| sort \
		| fzf -1 -0 -q ${1:- }`
	cd $dir
}

function m(){
	if [[ "$1" == "-r" ]]; then
		mv $2 ${2/.202?-??-??-??:??/}
	else
		mv "$1" "$1.`date +%Y-%m-%d-%H:%M`"
	fi
}
