# export TERM="xterm-256color"

export LANGUAGE=pl_PL.UTF-8
export LC_ALL=pl_PL.UTF-8
export LANG=pl_PL.UTF-8

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export FZF_DEFAULT_COMMAND='fd -i'

export EDITOR="/usr/bin/nvim"
export VISUAL=$EDITOR

export READER="/usr/bin/zathura"
export BROWSER="/usr/bin/opera"

export JDTLS_JVM_ARGS="-javaagent:$HOME/.local/share/java/lombok.jar"

# Hack for dotnet 6.0.300
export DOTNET_ROOT="$HOME/.local/lib/dotnet-6.0.300"
export PATH="$DOTNET_ROOT:$PATH:$DOTNET_ROOT"
