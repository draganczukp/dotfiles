#####################################################
# Filename: .zshrc									#
# Author: Przemek Dragańczuk <mail@draganczuk.tk>	#
# Description: Main ZSH config file					#
#####################################################


source ~/zsh/exports.zsh
source ~/zsh/path.zsh
source ~/zsh/history.zsh
source ~/zsh/keys.zsh
source ~/zsh/completions.zsh
# source ~/zsh/corrections.zsh
source ~/zsh/dirs.zsh
source ~/zsh/aliases.zsh
source ~/zsh/functions.zsh
source ~/zsh/prompt.zsh
source ~/zsh/sudo.zsh

setopt interactivecomments

autoload -U colors && colors

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/fzf/completion.zsh
source /usr/share/fzf/key-bindings.zsh

export TERMINAL="/usr/bin/kitty"

export EDITOR="/usr/bin/nvim"
export VISUAL=$EDITOR

export READER="/usr/bin/zathura"
export BROWSER="/usr/bin/opera"

export TERMINAL="/usr/local/bin/st"

[[ $commands[kubectl] ]] && source <(kubectl completion zsh)
AIO_AC_ZSH_SETUP_PATH=/home/przemek/.cache/@adobe/aio-cli/autocomplete/zsh_setup && test -f $AIO_AC_ZSH_SETUP_PATH && source $AIO_AC_ZSH_SETUP_PATH; # aio autocomplete setup

# bun completions
[ -s "/home/przemek/.bun/_bun" ] && source "/home/przemek/.bun/_bun"

# bun
export BUN_INSTALL="$HOME/.bun"
export PATH="$BUN_INSTALL/bin:$PATH"


# fnm
FNM_PATH="/home/przemek/.local/share/fnm"
if [ -d "$FNM_PATH" ]; then
  export PATH="/home/przemek/.local/share/fnm:$PATH"
  eval "`fnm env`"
fi

# asdf caue I want gleam
source "$HOME/.asdf/asdf.sh"
# append completions to fpath
fpath=(${ASDF_DIR}/completions $fpath)
# initialise completions with ZSH's compinit
autoload -Uz compinit && compinit
