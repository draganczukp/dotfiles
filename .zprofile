#!/bin/zsh

export EDITOR="/usr/bin/nvim"
export VISUAL=$EDITOR

export READER="/usr/bin/zathura"
export BROWSER="/usr/bin/firefox"

export TERMINAL="/usr/bin/kitty"

if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
	exec startx -- vt1 &> /tmp/startx.log
fi

# if [ -z "${WAYLAND_DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
#   exec sway --unsupported-gpu &> /tmp/sway.log
# fi
