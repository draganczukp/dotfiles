import os
import subprocess

from typing import List  # noqa: F401

from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen, ScratchPad, DropDown
from libqtile.lazy import lazy

mod = "mod4"
terminal = "alacritty"


@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~')
    subprocess.Popen([home + '/bin/startup'])


keys = [
    # Switch between windows
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),

    Key([mod], "h", lazy.layout.shrink(), desc="Shrink"),
    Key([mod], "l", lazy.layout.grow(), desc="Grow"),

    # Handled by SXHKD
    # Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.screen.toggle_group(), desc="Go back to last workspace"),
    Key([mod, "shift"], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod, "shift"], "q", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "shift"], "space", lazy.window.toggle_floating()),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    # Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    # Key([mod], "r", lazy.spawncmd(),
    # desc="Spawn a command using a prompt widget"),
    Key([mod], "b", lazy.hide_show_bar("all")),
]

groups = [
    ScratchPad("scratchpad", [
        DropDown("term", "alacritty")
    ]),
    Group("",
        matches=[
            Match(wm_class=["Firefox"]),
            Match(wm_class=["Opera"]),
            Match(wm_class=["Qutebrowser"])
        ], layout="monad_tall"),
    Group("", layout="monad_tall"),
    Group("", layout="monad_tall"),
    Group("", layout="monad_tall"),
    Group("", layout="monad_tall"),
    Group("", matches=[Match(wm_class="Steam")], layout="monad_tall"),
    Group("", matches=[Match(wm_class="discord")], layout="monad_tall"),
    Group("", layout="monad_tall"),
    Group("", matches=[Match(wm_class="spotify")], layout="monad_tall"),
]

for key, val in enumerate(groups, start=2):
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], str(key-2), lazy.group[val.name].toscreen(),
            desc="Switch to group {}".format(val.name)),

        Key([mod, "shift"], str(key-2), lazy.window.togroup(val.name),
            desc="move focused window to group {}".format(val.name)),
    ])

default_layout_conf = {
        "border_width": 1,
        "margin": 4,
        "border_focus": "#444444",
        "border_normal": "#111111"
        }

layouts = [
    layout.MonadTall(**default_layout_conf),
    layout.MonadWide(**default_layout_conf),
    layout.Max(**default_layout_conf),
]

widget_defaults = dict(
    font='DejaVuSansMono Nerd Font Mono',
    fontsize=13,
    padding=3,
    background="#333333",
    foreground="#fafafa",
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(highlight_method="block", block_highlight_text_color="#333333", this_current_screen_border="#fafafa", inactive="#888888"),
                widget.CurrentLayoutIcon(),
                widget.WindowName(),
                widget.Battery(battery=0, fmt="BAT0: {}", format="{percent:2.0%}"),
                # widget.Battery(battery=1, fmt="BAT1: {}", format="{percent:2.0%}"),
                widget.Clock(format='%H:%M:%S'),
                widget.Systray(),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
