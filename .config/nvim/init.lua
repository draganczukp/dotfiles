local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system {
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  }
end
vim.opt.rtp:prepend(lazypath)

require('lazy').setup({

  'svermeulen/vimpeccable',

  'tpope/vim-fugitive',

  'farmergreg/vim-lastplace',

  -- Detect tabstop and shiftwidth automatically
  'tpope/vim-sleuth',

  'tpope/vim-unimpaired',

  {
    'folke/tokyonight.nvim',
    lazy = false,
    priority = 1000,
  },

  {
    'kylechui/nvim-surround',
    opts = {},
  },

  { 'numToStr/Comment.nvim', opts = {} },

  { 'rmagatti/auto-session', opts = {} },

  'mbbill/undotree',

  {
    "eldritch-theme/eldritch.nvim",
    lazy = false,
    priority = 1000,
    opts = {},
  },

  {
    'stevearc/dressing.nvim',
    opts = {},
  },

  {
    'uga-rosa/ccc.nvim',
    opts = {},
  },

  {
    import = 'plugins',
  },

  {
    "tylopilus/sync-aem.nvim",
    opts = {},
    keys = {
      { "<leader>ae", function() require("sync-aem").export_file() end, desc = "Sync AEM - Export file" },
      { "<leader>ai", function() require("sync-aem").import_file() end, desc = "Sync AEM - Export file" },
    }
  },

  'folke/neodev.nvim',
}, {})

vim.cmd [[colorscheme tokyonight]]

require 'opts'

require 'keys'

require 'ft'

require('neodev').setup()

require 'debug_print'

-- vim: ts=2 sts=2 sw=2 et
