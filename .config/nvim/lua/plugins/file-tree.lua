return {
  'kyazdani42/nvim-tree.lua',
  dependencies = {
    'kyazdani42/nvim-web-devicons', -- optional, for file icon
  },
  config = function()
    require('nvim-tree').setup {
      on_attach = function(bufnr)
        local function opts(desc)
          return {
            desc = 'nvim-tree: ' .. desc,
            buffer = bufnr,
            noremap = true,
            silent = true,
            nowait = true,
          }
        end
        local ok, api = pcall(require, 'nvim-tree.api')
        assert(ok, 'api module is not found')
        api.config.mappings.default_on_attach(bufnr)
        vim.keymap.set('n', '<C-g>', function()
          require('nvim-tree.api').tree.find_file { open = true, current_window = true, focus = true }
        end, opts 'Find current file in tree')
      end,
    }
    vim.api.nvim_create_autocmd({ 'BufEnter' }, {
      pattern = 'NvimTree*',
      callback = function()
        local api = require 'nvim-tree.api'
        local view = require 'nvim-tree.view'
        if not view.is_visible() then
          api.tree.open()
        end
      end,
    })
  end,
  keys = {
    {
      '<Leader>f',
      function()
        require('nvim-tree.api').tree.toggle()
      end,
      desc = 'Toggle NvimTree',
    },
    {
      '<C-g>',
      function()
        require('nvim-tree.api').tree.find_file { open = true, current_window = true, focus = true }
      end,
      desc = 'Find current file in tree',
    }
  },
}
