return {
  "laytan/cloak.nvim",
  opts = {
    enabled = true,
    cloack_character = '*',
    highlight_group = 'Comment',
    cloak_length = 10, -- Provide a number if you want to hide the true length of the value.
    try_all_patterns = true,
    patterns = {
      {
        file_pattern = '.env*',
        cloak_pattern = '=.+',
        replace = nil,
      },
      {
        file_pattern = 'EPLAN*SECRET',
        cloak_pattern = '.+',
      }
    }
  },
}
