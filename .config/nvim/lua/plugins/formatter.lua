return {
  'stevearc/conform.nvim',
  keys = {
    {
      -- Customize or remove this keymap to your liking
      '<space>f',
      function()
        print('conform')
        require('conform').format { async = true, lsp_fallback = true }
      end,
      mode = '',
      desc = 'Format buffer',
    },
  },
  opts = {
    formatters = {
      superhtml = {
        inherit = false,
        command = 'superhtml',
        stdin = true,
        args = { 'fmt', '--stdin' },
      },
      ziggy = {
        inherit = false,
        command = 'ziggy',
        stdin = true,
        args = { 'fmt', '--stdin' },
      },
      ziggy_schema = {
        inherit = false,
        command = 'ziggy',
        stdin = true,
        args = { 'fmt', '--stdin-schema' },
      },
    },

    formatters_by_ft = {
      shtml = { 'superhtml' },
      ziggy = { 'ziggy' },
      ziggy_schema = { 'ziggy_schema' },
    },
  },
}
