return {
    'jakewvincent/mkdnflow.nvim',
    opts = {
        modules = {
            bib = false,
            cmp = false
        },
        to_do = {
            symbols = { ' ', '-', 'X' },
            update_parents = true,
            not_started = ' ',
            in_progress = '-',
            complete = 'X'
        },
    },
    keys = {
        {
            "<leader>td",
            function()
                vim.cmd.vsplit("~/Documents/todo/todo.md")
            end,
            desc = "Open TODO file"
        }
    }
}
