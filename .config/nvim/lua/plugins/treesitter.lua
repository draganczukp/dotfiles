vim.defer_fn(function()
  require('nvim-treesitter.configs').setup {
    -- Add languages to be installed here that you want installed for treesitter
    ensure_installed = {
      'bash',
      'comment',
      'css',
      'dockerfile',
      'gleam',
      'html',
      'java',
      'javascript',
      'json',
      'json5',
      'lua',
      'python',
      'rust',
      'scss',
      'sql',
      'tsx',
      'typescript',
      'vim',
      'vimdoc',
      'yaml',
    },

    -- Autoinstall languages that are not installed. Defaults to false (but you can change for yourself!)
    auto_install = true,

    highlight = { enable = true },
    indent = { enable = true },
    incremental_selection = {
      enable = true,
      keymaps = {
        init_selection = '<c-space>',
        node_incremental = '<c-space>',
        scope_incremental = '<c-s>',
        node_decremental = '<M-space>',
      },
    },
    textobjects = {
      select = {
        enable = true,
        lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
        keymaps = {
          -- You can use the capture groups defined in textobjects.scm
          ['aa'] = '@parameter.outer',
          ['ia'] = '@parameter.inner',
          ['af'] = '@function.outer',
          ['if'] = '@function.inner',
          ['ac'] = '@class.outer',
          ['ic'] = '@class.inner',
        },
      },
      move = {
        enable = true,
        set_jumps = true, -- whether to set jumps in the jumplist
        goto_next_start = {
          [']m'] = '@function.outer',
          [']]'] = '@class.outer',
        },
        goto_next_end = {
          [']M'] = '@function.outer',
          [']['] = '@class.outer',
        },
        goto_previous_start = {
          ['[m'] = '@function.outer',
          ['[['] = '@class.outer',
        },
        goto_previous_end = {
          ['[M'] = '@function.outer',
          ['[]'] = '@class.outer',
        },
      },
      swap = {
        enable = true,
        swap_next = {
          ['<leader>a'] = '@parameter.inner',
        },
        swap_previous = {
          ['<leader>A'] = '@parameter.inner',
        },
      },
    },
  }
  require('treesitter-context').setup {
    enable = true,           -- Enable this plugin (Can be enabled/disabled later via commands)
    max_lines = 2,           -- How many lines the window should span. Values <= 0 mean no limit.
    min_window_height = 0,   -- Minimum editor window height to enable context. Values <= 0 mean no limit.
    line_numbers = true,
    multiline_threshold = 2, -- Maximum number of lines to show for a single context
    trim_scope = 'outer',    -- Which context lines to discard if `max_lines` is exceeded. Choices: 'inner', 'outer'
    mode = 'cursor',         -- Line used to calculate context. Choices: 'cursor', 'topline'
    -- Separator between context and content. Should be a single character string, like '-'.
    -- When separator is set, the context will only show up when there are at least 2 lines above cursorline.
    separator = nil,
    zindex = 20,     -- The Z-index of the context window
    on_attach = nil, -- (fun(buf: integer): boolean) return false to disable attaching
  }

  local parser_config = require(
    'nvim-treesitter.parsers'
  ).get_parser_configs()

  parser_config.ziggy = {
    install_info = {
      url = 'https://github.com/kristoff-it/ziggy',
      includes = { 'tree-sitter-ziggy/src' },
      files = { 'tree-sitter-ziggy/src/parser.c' },
      branch = 'main',
      generate_requires_npm = false,
      requires_generate_from_grammar = false,
    },
    filetype = 'ziggy',
  }

  parser_config.ziggy_schema = {
    install_info = {
      url = 'https://github.com/kristoff-it/ziggy',
      files = { 'tree-sitter-ziggy-schema/src/parser.c' },
      branch = 'main',
      generate_requires_npm = false,
      requires_generate_from_grammar = false,
    },
    filetype = 'ziggy-schema',
  }

  parser_config.supermd = {
    install_info = {
      url = 'https://github.com/kristoff-it/supermd',
      includes = { 'tree-sitter/supermd/src' },
      files = {
        'tree-sitter/supermd/src/parser.c',
        'tree-sitter/supermd/src/scanner.c'
      },
      branch = 'main',
      generate_requires_npm = false,
      requires_generate_from_grammar = false,
    },
    filetype = 'supermd',
  }

  parser_config.supermd_inline = {
    install_info = {
      url = 'https://github.com/kristoff-it/supermd',
      includes = { 'tree-sitter/supermd-inline/src' },
      files = {
        'tree-sitter/supermd-inline/src/parser.c',
        'tree-sitter/supermd-inline/src/scanner.c'
      },
      branch = 'main',
      generate_requires_npm = false,
      requires_generate_from_grammar = false,
    },
    filetype = 'supermd_inline',
  }

  parser_config.superhtml = {
    install_info = {
      url = 'https://github.com/kristoff-it/superhtml',
      includes = { 'tree-sitter-superhtml/src' },
      files = {
        'tree-sitter-superhtml/src/parser.c',
        'tree-sitter-superhtml/src/scanner.c'
      },
      branch = 'main',
      generate_requires_npm = false,
      requires_generate_from_grammar = false,
    },
    filetype = 'superhtml',
  }

  vim.filetype.add {
    extension = {
      smd = 'supermd',
      shtml = 'superhtml',
      ziggy = 'ziggy',
      ['ziggy-schema'] = 'ziggy_schema',
    },
  }
end, 0)

vim.cmd [[
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()
set nofoldenable                     " Disable folding at startup.
]]

return {
  -- Highlight, edit, and navigate code
  'nvim-treesitter/nvim-treesitter',
  dependencies = {
    'nvim-treesitter/nvim-treesitter-textobjects',
    'nvim-treesitter/nvim-treesitter-context',
  },
  build = ':TSUpdate',
}
