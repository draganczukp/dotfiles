return {
  -- Set lualine as statusline
  'nvim-lualine/lualine.nvim',
  -- See `:help lualine.txt`

  opts = {
    options = {
      icons_enabled = true,
      theme = 'tokyonight',
      component_separators = { right = '', left = '' },
      section_separators = { right = '', left = '' },
    },
    tabline = {
      lualine_a = { {'tabs', mode = 1, max_length = vim.o.columns} },
      lualine_b = {},
      lualine_c = {},
      lualine_x = {},
      lualine_y = {},
      lualine_z = {},
    },
    sections = {
      lualine_a = {
        { 'mode', right_padding = 2, icons_enabled = true },
      },
      lualine_b = { 'filename'},
      lualine_c = { { 'diagnostics', sources = { 'nvim_lsp' } } },
      lualine_x = { { 'diff' }, 'branch' },
      lualine_y = { { 'searchcount' }, 'filetype', 'progress' },
      lualine_z = {
        { 'location', left_padding = 2 },
      },
    },
  },
}
