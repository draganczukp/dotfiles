return {
  "ptdewey/yankbank-nvim",
  opts = {},
  keys = {
    { "<leader>y", "<cmd>YankBank<cr>", desc = "Yank bank" },
  }
}
