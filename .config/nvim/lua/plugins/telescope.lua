vim.defer_fn(function()
  require('telescope').setup {
    defaults = {
      mappings = {
        i = {
          ['<C-u>'] = false,
          ['<C-d>'] = false,
        },
      },
      extensions = {
        ["ui-select"] = {
          require("telescope.themes").get_dropdown {
            -- even more opts
          }
        }
      }

    },
  }

  -- Enable telescope fzf native, if installed
  pcall(require('telescope').load_extension, 'fzf')
end, 0)

return {
  'nvim-telescope/telescope.nvim',
  branch = '0.1.x',
  dependencies = {
    'nvim-lua/plenary.nvim',
    {
      'nvim-telescope/telescope-ui-select.nvim',
      config = function()
        require("telescope").load_extension("ui-select")
      end
    },
    {
      'nvim-telescope/telescope-fzf-native.nvim',
      build = 'make',
      cond = function()
        return vim.fn.executable 'make' == 1
      end,
    },
    {
      'danielfalk/smart-open.nvim',
      branch = '0.1.x',
      config = function()
        require('telescope').load_extension 'smart_open'
      end,
      keys = {
        {
          '<C-p>',
          function()
            require('telescope').extensions.smart_open.smart_open { cwd_only = true }
          end,
          desc = 'Smart Open',
        },
      },
    },
    { 'kkharji/sqlite.lua' },
  },
  keys = {
    {
      '<C-f>',
      function()
        require('telescope.builtin').live_grep()
      end,
      desc = 'Find Text',
    },
    {
      '<C-b>',
      function()
        require('telescope.builtin').buffers()
      end,
      desc = 'Find Text',
    },
    {
      '<leader>/',
      function()
        require('telescope.builtin').current_buffer_fuzzy_find(require('telescope.themes').get_dropdown {
          winblend = 10,
          previewer = false,
        })
      end,
      desc = '[/] Fuzzily search in current buffer',
    },
  },
}
