vim.api.nvim_create_autocmd('BufRead', {
  pattern = {'*/dispatcher/*.conf', '*/dispatcher/*.htaccess', '*/dispatcher/*.vhost', '*/dispatcher/*.rules'},
  callback = function()
    vim.bo.filetype = 'apache'
  end
})

vim.api.nvim_create_autocmd('BufRead', {
  pattern = {'*.html'},
  callback = function()
    vim.bo.tabstop = 4
  end
});

vim.api.nvim_create_autocmd({'BufRead', 'BufNewFile', 'BufEnter'}, {
  pattern = {'*.js', '*.ts', '*.jsx', '*.tsx'},
  callback = function()
    vim.bo.tabstop = 2
  end
})
