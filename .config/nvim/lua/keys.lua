vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- My
local vimp = require("vimp");

vimp.nnoremap('<Esc>', ':noh<return><esc>')

vimp.tnoremap('<C-x>', '<C-\\><C-n>')

vimp.nnoremap('<C-j>', '<C-W>j')
vimp.nnoremap('<C-k>', '<C-W>k')
vimp.nnoremap('<C-h>', '<C-W>h')
vimp.nnoremap({'override'}, '<C-l>', '<C-W>l')

vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

vimp.nnoremap('<S-Left>', ':vertical resize -1<CR>')
vimp.nnoremap('<S-Right>', ':vertical resize +1<CR>')
vimp.nnoremap('<S-Up>', ':resize -1<CR>')
vimp.nnoremap('<S-Down>', ':resize 1<CR>')

vimp.nnoremap('<Leader>a', 'ggVG')
vimp.nnoremap('<Leader>g', 'mggg=G1g:StripWhitespace<CR>')


vim.cmd([[
cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Qall! qall!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev W w
]])

