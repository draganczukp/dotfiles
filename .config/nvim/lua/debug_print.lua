-- Uses treesitter to see if there's a logger
local function java_log()
  local query_text = [[
    (marker_annotation name: (identifier) @annotation_name (#any-of? @annotation_name "Slf4j" "Log" "Log4j" "Logger"))
    ]]
  local lang = require('nvim-treesitter.parsers').ft_to_lang('java')
  local query = vim.treesitter.query.parse(lang, query_text)
  print(vim.inspect(query))

  for _ in query:iter_captures(vim.treesitter.get_parser():parse()[1]:root(), 0) do
    return 'log.debug("%s: {}", %s);'
  end

  return 'System.out.println("%s: " + %s);'
end
function DebugPrint(above)
  -- TODO: Use treesitter
  local word = vim.fn.expand('<cword>')
  local ft = vim.bo.ft:lower()

  local statent_template

  if ft == 'java' then
    -- TODO: Look for a debugger
    statent_template = java_log()
  elseif ft == 'lua' then
    statent_template = 'print("%s: ", %s)'
  elseif ft == 'typescript' or ft == 'javascript' then
    statent_template = 'console.log("%s: ", %s)'
  end

  if statent_template then
    local statent = string.format(statent_template, word, word)

    local cursor = vim.api.nvim_win_get_cursor(0)
    local row, _ = unpack(cursor)

    if above then
      row = row - 1
    end

    vim.api.nvim_buf_set_lines(0, row, row, false, { statent })
  end
end

vim.keymap.set('n', '<leader>dp', function()
  DebugPrint(false)
end)

vim.keymap.set('n', '<leader>dP', function()
  DebugPrint(true)
end)
